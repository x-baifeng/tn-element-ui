import path from 'path'

import vue from 'rollup-plugin-vue'
import alias from '@rollup/plugin-alias'
import babel from '@rollup/plugin-babel'
import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'

import { terser } from 'rollup-plugin-terser'
import { string } from 'rollup-plugin-string'

import postcss from 'rollup-plugin-postcss'
import postcssAutoprefixer from 'autoprefixer'
import postcssCalc from 'postcss-calc'
import postcssClean from 'postcss-clean'
import postcssConditionals from 'postcss-conditionals'
import postcssCustomProps from './build/postcss-custom-properties-polyfill'
import postcssFor from 'postcss-for'
import postcssImport from 'postcss-import'
import postcssNested from 'postcss-nested'
import postcssVars from 'postcss-simple-vars'
import postcssUnprefix from 'postcss-unprefix'
import postcssMixins from 'postcss-mixins'

import variables from './src/variables'

const version = process.env.VERSION || require('./package.json').version

const banner = `
/* !
 * library v${version}
 * https://gitee.com/x-baifeng/tn-element-ui
 * 
 * (c) ${new Date().getFullYear()} xiaobaifeng
 */
`
const baseOutputOpts = {
  globals: {
    vue: 'Vue',
    'element-ui': 'ELEMENT'
  },
  sourcemap: false,
  banner
}

const isDevEnv = process.env.dev

const output = [
  {
    ...baseOutputOpts,
    file: 'dist/tn-element-ui.js',
    format: 'umd',
    name: 'tnElementUi'
  }
].concat(
  isDevEnv
    ? undefined
    : [
        {
          ...baseOutputOpts,
          file: 'dist/tn-element-ui.esm.js',
          format: 'esm'
        },
        {
          ...baseOutputOpts,
          file: 'dist/tn-element-ui.min.js',
          format: 'umd',
          name: 'tn-element-ui',
          plugins: isDevEnv ? undefined : [terser()]
        },
        {
          ...baseOutputOpts,
          file: 'dist/tn-element-ui.esm.min.js',
          format: 'esm',
          plugins: isDevEnv ? undefined : [terser()]
        }
      ]
)

export default {
  input: 'src/index.js',
  output,
  external: ['vue', 'element-ui'],
  plugins: [
    alias({
      entries: [
        { find: '@icons', replacement: '@tabler/icons/icons' },
        { find: '@utils', replacement: path.resolve(__dirname, 'src/utils') },
        { find: '@mixins', replacement: path.resolve(__dirname, 'src/mixins') },
        { find: '@events', replacement: path.resolve(__dirname, 'src/events') }
      ]
    }),
    vue({
      css: false
    }),
    string({
      include: '**/*.svg'
    }),
    postcss({
      plugins: [
        postcssImport,
        postcssMixins,
        postcssUnprefix,
        postcssFor,
        postcssVars({ variables }),
        postcssCustomProps(),
        postcssCalc,
        postcssNested,
        postcssConditionals,
        postcssAutoprefixer,
        postcssClean(
          isDevEnv
            ? {
                format: {
                  breaks: {
                    afterAtRule: true,
                    afterBlockBegins: true,
                    afterBlockEnds: true,
                    afterComment: true,
                    afterProperty: true,
                    afterRuleBegins: true,
                    afterRuleEnds: true,
                    beforeBlockEnds: true,
                    betweenSelectors: true
                  },
                  spaces: {
                    aroundSelectorRelation: true,
                    beforeBlockBegins: true,
                    beforeValue: true
                  },
                  semicolonAfterLastProperty: true,
                  indentBy: 2
                }
              }
            : undefined
        )
      ]
      // extract: path.resolve(__dirname, 'dist/mussel.css')
    }),
    resolve({
      mainFields: ['module', 'main', 'browser']
    }),
    babel({
      babelHelpers: 'bundled',
      exclude: 'node_modules/**',
      extensions: ['.js', '.jsx', '.es6', '.es', '.mjs', '.vue']
    }),
    commonjs()
  ],
  onwarn: (warning) => {
    const { code, plugin, id, input, message, text } = warning
    console.warn('[!]', '[B]', code || warning)
    if (plugin) console.warn('[!]', '...', '[plugin]', plugin)
    if (id) console.warn('[!]', '...', '[id]', id)
    if (input) console.warn('[!]', '...', '[input]', input.file || input)
    if (message) console.warn('[!]', '...', '[message]', message)
    if (text) console.warn('[!]', '...', '[message]', text)
  }
}
