# [1.3.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.2.3...v1.3.0) (2022-08-03)


### Features

* **examples:** add editor ([2fb178b](https://gitee.com/x-baifeng/tn-element-ui/commits/2fb178b0213f087e3e3064a61d2af3e7196752cc))
* **examples:** example.html add components sidebar ([b4784dc](https://gitee.com/x-baifeng/tn-element-ui/commits/b4784dc61e6a3125dd06df855d4e0c5db0573c3b))
* **examples:** example.html add editor btn switch ([0124d34](https://gitee.com/x-baifeng/tn-element-ui/commits/0124d34564b19ad521021c2e5fb4303250bec1f2))



## [1.2.3](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.1.3...v1.2.3) (2022-06-14)


### Features

* **filters:** add dateFormatFilter ([9374227](https://gitee.com/x-baifeng/tn-element-ui/commits/93742273b34fe603ac6a0bd9e9d44686a3cca27f))
* **filters:** dataFormFilter add format week ([97883e6](https://gitee.com/x-baifeng/tn-element-ui/commits/97883e6e0422358a08c907ff4ea2945996c35954))
* **utils:** add createArray,createPromise,abortPromise ([0f61c52](https://gitee.com/x-baifeng/tn-element-ui/commits/0f61c52fd761b798a9af68696cf525d846d32d98))
* **visual-ui:** 新增时间轴播放条组件 ([84ea81b](https://gitee.com/x-baifeng/tn-element-ui/commits/84ea81b2f24539d36b32668b609801bd5db15cec))



## [1.1.3](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.1.2...v1.1.3) (2022-04-15)


### Features

* **StretchWrapper:** add arrow-right-icon slot ([3f79547](https://gitee.com/x-baifeng/tn-element-ui/commits/3f7954742aa521b42fcc0262ac26a24c1bf08e7e))



## [1.1.2](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.1.1...v1.1.2) (2022-04-14)


### Bug Fixes

* **TnElTable:** data change bodyWrapper scroll reset ([35ae26a](https://gitee.com/x-baifeng/tn-element-ui/commits/35ae26acd9b5a6dd6409e7c71b32c2cea5f39e2f))



## [1.1.1](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.1.0...v1.1.1) (2022-03-30)


### Features

* **Layout:** add tn-no-flex ([7c238b0](https://gitee.com/x-baifeng/tn-element-ui/commits/7c238b042120388599a7b18899eabc782c7d6daa))



# [1.1.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.1.0-beta.0...v1.1.0) (2022-03-30)



# [1.1.0-beta.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v1.0.0...v1.1.0-beta.0) (2022-03-22)


### Bug Fixes

* **StretchWrapper:** 默认收起状态是borderWidth: 0 ([2096afc](https://gitee.com/x-baifeng/tn-element-ui/commits/2096afcec20a1cc9800defd9f01663d412c0f03a))


### Features

* 新增伸缩包裹器组件 ([d60a856](https://gitee.com/x-baifeng/tn-element-ui/commits/d60a8568f4cd77673db873565a2d38e9c7aa59a5))



# [1.0.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.2.0...v1.0.0) (2022-02-25)


### Features

* 移除TnPinElbadge,补充tab-panel slot label文档 ([b168b8f](https://gitee.com/x-baifeng/tn-element-ui/commits/b168b8f90a1aeef97cd3ce5b8e05417655ce7401))



# [0.2.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.1.0...v0.2.0) (2022-02-17)


### Bug Fixes

* **Layout:** 移除table上的cellspacing,cellpadding样式 ([485cb49](https://gitee.com/x-baifeng/tn-element-ui/commits/485cb49c9089548518820ba87ab47be4b1eb7503))
* **Layout:** pcss size='1|8'选择器样式错误 ([79270e4](https://gitee.com/x-baifeng/tn-element-ui/commits/79270e49d6642cf95feb9b2356aed2d42739bd27))


### Features

* 对外暴露扩充的layout相关组件 ([68e3d72](https://gitee.com/x-baifeng/tn-element-ui/commits/68e3d72e4a1170a52776ad793d3743db1b490703))
* 对外释放horn自定义指令 ([78faa79](https://gitee.com/x-baifeng/tn-element-ui/commits/78faa791e3edb3984e0dc1f06ae10638e871507c))
* 添加布局相关组件 ([885556a](https://gitee.com/x-baifeng/tn-element-ui/commits/885556a8b449c980baf6254e178200480b942a3c))
* 添加角指令v-horn ([f727c90](https://gitee.com/x-baifeng/tn-element-ui/commits/f727c907a6c963cfb0d6aeb82067e1b33a37430b))
* 添加base-styles ([fbc4149](https://gitee.com/x-baifeng/tn-element-ui/commits/fbc4149e4450b16fa1de395825be49377901c75d))
* 添加TnPinElbadge组价 ([b898a00](https://gitee.com/x-baifeng/tn-element-ui/commits/b898a001022ae35b002465ee4198b7befa571025))
* **Layout:** 移除table上的cellspacing,cellpadding样式 ([4648bff](https://gitee.com/x-baifeng/tn-element-ui/commits/4648bff9579621b8566bf94f9e55ccec8e88dc11))



# [0.1.0](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.14...v0.1.0) (2022-02-10)


### Bug Fixes

* **TnEltable:** 列配置长度变大时表格样式错误 ([484ca48](https://gitee.com/x-baifeng/tn-element-ui/commits/484ca485fd963cf5441dd5a1dd8c0ad1f6d3690c))
* **TnEltable:** columnConfig.caption(string)失效 ([a31d740](https://gitee.com/x-baifeng/tn-element-ui/commits/a31d740d7a23189888f0917ad7e653482934075d))



## [0.0.14](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.13...v0.0.14) (2022-01-18)


### Features

* **TnEltable:** 列配置支持render ([01e145d](https://gitee.com/x-baifeng/tn-element-ui/commits/01e145d7678f2b443ea8fca202c29c5a909f4676))
* **TnEltable:** buttonColumn 添加emptyCellValue ([ae8b128](https://gitee.com/x-baifeng/tn-element-ui/commits/ae8b128207384dada31fb4faf16c6572152f8a7e))



## [0.0.13](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.12...v0.0.13) (2021-12-22)


### Bug Fixes

* **TnEltable:** 检验空单元格逻辑错误的缺陷 ([a067d6b](https://gitee.com/x-baifeng/tn-element-ui/commits/a067d6b274860606db4e3eb3f47bd6d2a5628102))



## [0.0.12](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.11...v0.0.12) (2021-12-01)


### Bug Fixes

* **TnEltabl:** 按钮列初始化不显示 ([086a4ab](https://gitee.com/x-baifeng/tn-element-ui/commits/086a4ab828e2e071d6b32eacdad359273d342bf4))



## [0.0.11](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.10...v0.0.11) (2021-12-01)


### Bug Fixes

* **TnEltabl:** 按钮列hidden return null ([2a4bb0e](https://gitee.com/x-baifeng/tn-element-ui/commits/2a4bb0e0f5fdc37da15538d3e0a1367b3e757ebb))



## [0.0.10](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.9...v0.0.10) (2021-12-01)


### Features

* **TnEltabl:** 按钮列支持hidden: Function ([b84cbbb](https://gitee.com/x-baifeng/tn-element-ui/commits/b84cbbbe92ca49c70f8352e569a2a9500528df55))



## [0.0.9](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.8...v0.0.9) (2021-11-26)


### Features

* **TnEltable:** cut table siblings height ([fc2cc15](https://gitee.com/x-baifeng/tn-element-ui/commits/fc2cc156fd4897031b883058e6e39924916c00e8))



## [0.0.8](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.7...v0.0.8) (2021-11-26)


### Features

* **TnEltable:** detect parantVm resize ([4d4b7e9](https://gitee.com/x-baifeng/tn-element-ui/commits/4d4b7e9eb683191ca592fde5c7e64935a69bb7c7))



## [0.0.7](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.6...v0.0.7) (2021-11-19)



## [0.0.6](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.5...v0.0.6) (2021-11-19)


### Features

* **TnEltabl:** add tableVm ([9902ea5](https://gitee.com/x-baifeng/tn-element-ui/commits/9902ea5797491a152670d718e4d479e1133776ab))



## [0.0.5](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.4...v0.0.5) (2021-11-18)


### Features

* **TnEltable:** add slot,like el-table slot ([c13e58a](https://gitee.com/x-baifeng/tn-element-ui/commits/c13e58a85411ff591a34345473da2f31b1d75a0f))



## [0.0.4](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.3...v0.0.4) (2021-11-17)


### Features

* **TnEltable:** add prop: autoHeight ([638ef3d](https://gitee.com/x-baifeng/tn-element-ui/commits/638ef3d5375f7483723a5494ec086eff3b640ce9))



## [0.0.3](https://gitee.com/x-baifeng/tn-element-ui/compare/v0.0.2...v0.0.3) (2021-11-17)


### Features

* **TnEltable:** 支持 el-table props ([a8a618b](https://gitee.com/x-baifeng/tn-element-ui/commits/a8a618b1bc5519372d957d91074d4a606518a4d4))



## [0.0.2](https://gitee.com/x-baifeng/tn-element-ui/compare/36842995d48aa671c2c7a77bf8ee3609d7dcb880...v0.0.2) (2021-11-17)


### Features

* **TnEltable:** init,add props:columns,emptyCellValue ([3684299](https://gitee.com/x-baifeng/tn-element-ui/commits/36842995d48aa671c2c7a77bf8ee3609d7dcb880))



