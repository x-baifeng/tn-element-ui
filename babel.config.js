module.exports = api => {
  const isTest = api.env('test');
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          modules: isTest ? "auto" : false,
          useBuiltIns: 'usage',
          corejs: 3,
          targets: { node: 'current' }
        }
      ]
    ],
    plugins: [
      '@vue/babel-plugin-transform-vue-jsx'
    ]
  }
}
