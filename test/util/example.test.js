import { mount } from '@vue/test-utils'
import HelloWorld from './HelloWorld.vue'

describe('TestVue', () => {
  // Now mount the component and you have the wrapper
  const msg = 'new message'
  const wrapper = mount(HelloWorld, {
    propsData: {
      msg
    }
  })

  it('renders a HelloWorld component', () => {
    const helloWorld = wrapper.findComponent(HelloWorld) // => 通过组件实例找到 HelloWorld
    expect(helloWorld.exists()).toBe(true)
  })

  it('renders props.msg when passed', () => {
    expect(wrapper.text()).toMatch(msg)
  })
})
