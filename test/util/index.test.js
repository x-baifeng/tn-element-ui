import { validateParam } from '@utils'

/**
 * @typedef {object} testParams
 * @property {any} value 值
 * @property {boolean} result 结果
 */

describe('validateParam', () => {
  it.each`
    value             | result
    ${null}           | ${false}
    ${undefined}      | ${false}
    ${0}              | ${true}
    ${1}              | ${true}
    ${"''"}           | ${true}
    ${'1'}            | ${true}
    ${true}           | ${true}
    ${false}          | ${true}
    ${{}}             | ${true}
    ${function () {}} | ${true}
    ${[]}             | ${true}
  `(
    '$value - $result',
    /**
     * @param {testParams} testParams
     */
    ({ value, result }) => {
      expect(validateParam(value)).toBe(result)
    }
  )
})
