const fs = require('fs')
const path = require('path')

class JestTransformMochawesomeReporter {
  constructor (globalConfig, options) {
    this._globalConfig = globalConfig
    this._options = options
  }

  onRunComplete (contexts, results) {
    // console.log('Custom reporter output:');
    // console.log('GlobalConfig: ', this._globalConfig);
    // console.log('Options: ', this._options)
    // console.log('results:', results)
    const mockMochawesomeResults = {
      stats: {
        passes: results.numPassedTests,
        tests: results.numTotalTests,
        pending: results.numPendingTestss,
        failures: results.numFailedTests,
        skipped: 0,
        passPercent: Math.floor(
          results.numPassedTests / results.numTotalTests * 10000
        ) / 100
      }
    }
    fs.writeFileSync(
      path.join(this._globalConfig.coverageDirectory, 'jest-custom-reports__transform-mochawesome.json'),
      JSON.stringify(mockMochawesomeResults, null, '\t'),
      function (err) {
        if (err) console.error('write file err', err)
      }
    )
  }
}

module.exports = JestTransformMochawesomeReporter
