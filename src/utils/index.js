export function validateParam (v) {
  return v !== null && v !== undefined
}

/**
 * Query an element selector if it's not an element already.
 * @param { string | Element } el
 * @return { Element }
 */
export function query (el) {
  if (typeof el === 'string') {
    const selected = document.querySelector(el)
    if (!selected) {
      process.env.NODE_ENV !== 'production' && console.warn(
        'Cannot find element: ' + el
      )
      return document.createElement('div')
    }
    return selected
  } else {
    return el
  }
}

export function createArray (num, mapCB) {
  return Array(num).fill().map(
    mapCB || function (item, index) {
      return index
    }
  )
}

let timer

export function createPromise (fn, speed) {
  return new Promise(resolve => {
    timer = setTimeout(function () {
      fn()
      resolve()
    }, speed)
  })
}

export function abortPromise (promise) {
  clearTimeout(timer)
  const abort = new Promise(function (resolve, reject) {
    reject(new Error('the promise is aborted'))
  })
  Promise.race([promise, abort])
    .then(console.log)
    .catch(e => {
      console.log(e)
    })
}
