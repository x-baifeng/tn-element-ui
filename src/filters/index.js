import dayjs from 'dayjs'

export function dateFormat (value, format = 'YYYY-MM-DD HH:mm:ss') {
  if (format === 'week') {
    return '周' + weekToHanZi(dayjs(value).day())
  }
  return dayjs(value).format(format)
}

export function weekToHanZi (value) {
  switch (value) {
    case 1:
      return '一'
    case 2:
      return '二'
    case 3:
      return '三'
    case 4:
      return '四'
    case 5:
      return '五'
    case 6:
      return '六'
    case 0:
      return '日'
    default:
      break
  }
}

export function installFilters (Vue) {
  Vue.filter('dateFormatFilter', dateFormat)
}
