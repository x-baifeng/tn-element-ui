export default {
  name: 'TnRenderCell',
  functional: true,
  props: {
    row: Object,
    index: Number,
    column: {
      type: Object,
      default: null
    }
  },
  render: (h, ctx) => {
    return ctx.props.column.render.call(null, h, ctx.props)
  }
}
