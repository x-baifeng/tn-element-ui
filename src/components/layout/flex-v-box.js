import FlexBox from './flex-box.vue'

export default {
  name: 'TinyVBox',
  extends: FlexBox,
  props: ['layout', 'direction'],
  computed: {
    flexLayout () {
      return 'column'
    }
  }
}
