import FlexBox from './flex-box.vue'

export default {
  name: 'TinyHBox',
  extends: FlexBox,
  props: ['layout', 'direction'],
  computed: {
    flexLayout () {
      return 'row'
    }
  }
}
