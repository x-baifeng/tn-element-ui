/* TnEltable */
import TnEltable from './tn-eltable/index.vue'
/* LAYOUT */
import FlexBox from './layout/flex-box.vue'
import FlexItem from './layout/flex-item.vue'
import HBox from './layout/flex-h-box'
import VBox from './layout/flex-v-box'
import Space from './layout/space.vue'
import Splitter from './layout/splitter.vue'
import StretchWrapper from './wrappers/stretch-wrapper.vue'
import TimeAxisPlayBar from './visual-ui/time-axis-play-bar/index.vue'

export {
  TnEltable,
  FlexBox,
  FlexItem,
  HBox,
  VBox,
  Space,
  Splitter,
  StretchWrapper,
  TimeAxisPlayBar
}

export function installComponents (Vue) {
  Vue.component('TnEltable', TnEltable)

  Vue.component('TnFlexBox', FlexBox)
  Vue.component('TnFlexItem', FlexItem)
  Vue.component('TnHBox', HBox)
  Vue.component('TnVBox', VBox)
  Vue.component('TnSpace', Space)
  Vue.component('TnSplitter', Splitter)
  Vue.component('TnStretchWrapper', StretchWrapper)
  Vue.component('TnTimeAxisPlayBar', TimeAxisPlayBar)
}
