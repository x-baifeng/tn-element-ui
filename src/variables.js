export default {
  /* language */
  /* lang = 'zh' */

  /* spacing */
  halfSpacingSize: '8px',
  unitSpacingSize: '16px',

  /* primary colors */
  primaryColor: '#1890ff',
  primaryPlusColor: '#096dd9',
  primaryMinusColor: '#40a9ff',
  primaryTinyColor: '#bae7ff',
  primaryShadowColor: 'rgba(24, 144, 255, .15)',

  /* success colors */
  successColor: '#52c41a',
  successPlusColor: '#389e0d',
  successMinusColor: '#73d13d',
  successTinyColor: '#d9f7be',
  successShadowColor: 'rgba(82, 196, 26, .15)',

  /* danger colors */
  dangerColor: '#f5222d',
  dangerPlusColor: '#cf1322',
  dangerMinusColor: '#ff4d4f',
  dangerTinyColor: '#ffccc7',
  dangerShadowColor: 'rgba(245, 34, 45, .15)',

  /* warning colors */
  warningColor: '#faad14',

  /* font */
  documentFontSize: '14px',
  documentFontWeight: '400',

  /* normal control vars */

  /* text color */
  normalTextColor: '#666',
  normalWeakTextColor: '#bbb',
  normalTitleTextColor: '#555',
  normalSubtitleTextColor: '#777',
  normalDisabledTextColor: '#bbb',
  normalHighlightTextColor: '#fff',

  /* line color */
  normalBorderColor: '#d6d6d6',
  normalDividerColor: '#e0e0e0',

  /* background */
  normalBackground: '#fff',
  normalGreyBackground: '#e9e9e9',
  normalHoverBackground: '#ececec',
  normalDarkGreyBackground: '#a6a6a6',
  normalDisabledBackground: '#e6e6e6',
  normalInfoBackground: '#ffd',

  /* element size */
  normalBarHeight: '40px',
  compactBarHeight: '32px',
  normalLineHeight: '20px',
  normalIconWidth: '20px',

  /* shadow */
  boxShadowLevel1:
    '0 1.5px 3px rgba(0, 0, 0, 0.25), 0 1.5px 6px rgba(0, 0, 0, 0.12)',
  boxShadowLevel2:
    '0 3px 6px rgba(0, 0, 0, 0.25), 0 3px 12px rgba(0, 0, 0, 0.15)',
  boxShadowLevel3:
    '0 6px 12px rgba(0, 0, 0, 0.25), 0 6px 24px rgba(0, 0, 0, 0.18)',

  /* layout & splitter */
  splitterSize: '1px',
  splitterColor: '$normalDividerColor',
  splitterHoverColor: '$normalBorderColor',

  /* horn */
  hornWidth: '18px',
  hornHeight: '18px',
  hornOffset: '-6px',
  hornBorderWidth: '1px',
  hornBorderColor: '#333333'
}
