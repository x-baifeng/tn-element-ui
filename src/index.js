import Vue from 'vue'

import './base-styles'

import { installDirectives } from './directives'
import { installComponents } from './components'
import { installFilters } from './filters'

export * from './components'
export * from './directives'

export function install ($Vue) {
  installDirectives($Vue)
  installComponents($Vue)
  installFilters($Vue)
}

if (Vue) install(Vue)
