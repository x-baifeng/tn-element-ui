function showOrHideHorns (el, value) {
  el.classList[value ? 'add' : 'remove']('show')
}

function updateHornEmitType (el, binding, vnode) {
  // 支持 click, hover
  const { value, expression, modifiers } = binding
  if (value !== undefined || expression !== undefined) {
    el.removeEventListener('mouseenter', function () {
      showOrHideHorns(el, true)
    })
    el.removeEventListener('mouseleave', function () {
      showOrHideHorns(el, false)
    })
    el.removeEventListener('click', function () {
      showOrHideHorns(el, el.classList.contains('show'))
    })
    showOrHideHorns(el, value)
    return
  }
  let action = null
  if (value === undefined && expression === undefined) {
    action = modifiers.click ? 'click' : 'hover'
  }
  if (action === 'hover') {
    el.addEventListener('mouseenter', function () {
      showOrHideHorns(el, true)
    })
    el.addEventListener('mouseleave', function () {
      showOrHideHorns(el, false)
    })
  } else if (action === 'click') {
    el.addEventListener('click', function () {
      showOrHideHorns(el, !el.classList.contains('show'))
    })
  }
}

export default {
  bind: function (el, binding, vnode) {
    el.classList.add('tn-horn-container')
    const hornPositionNames = ['left-top', 'right-top', 'left-bottom', 'right-bottom']
    hornPositionNames.forEach(positionName => {
      const hornItemDom = document.createElement('div')
      hornItemDom.classList.add('tn-horn', positionName)
      // 这里不可以使用 属性
      // 因为这里dom 会被变换为 Vm
      // hornItemDom.setAttribute(item)
      el.appendChild(hornItemDom)
    })
    updateHornEmitType(el, binding, vnode)
  },
  inserted: function (el, binding) {
  },
  update: function (el, binding, vnode) {
    updateHornEmitType(el, binding, vnode)
  },
  componentUpdated: function () {},
  unbind: function () {}
}
