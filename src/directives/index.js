import horn from './horn'

function installDirectives (Vue) {
  Vue.directive('horn', horn)
}

export {
  horn,
  installDirectives
}
