module.exports = {
  moduleFileExtensions: ['js', 'json', 'vue'],
  transform: {
    '.*\\.(vue)$': 'vue-jest',
    '\\.[jt]sx?$': 'babel-jest'
  },
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '^@component/(.*)$': '<rootDir>/src/component/$1',
    '^@utils/?(.*)$': '<rootDir>/src/utils/$1',
    '^@test/(.*)$': '<rootDir>/test/$1'
  },
  setupFilesAfterEnv: ['./jest.setup.js'],
  collectCoverage: true,
  // 查看详细输出
  verbose: true,
  // TODO: 自定义报告器 MyCustomReporter 
  // https://www.jestjs.cn/docs/configuration#collectcoverage-boolean
  reporters: ['default', './jest-custom-reports/transform-mochawesome-reporter.js']
}
