# TnTimeAxisPlayBar

时间坐标轴播放条

### API

#### Name

- 导出名称: `TimeAxisPlayBar`
- 组件名称: `TinyVisualUiTimeAxisPlayBar`
- 标签名成: `tn-time-axis-play-bar`

#### Attributes

| 名称          | 描述           | 类型    | 可选值 | 默认值 |
| ------------- | -------------- | ------- | ------ | ------ |
| visible       | 是否显示(sync) | Boolean | -      | false  |
| rangeStart    | 开始时间       | Date    | -      |        |
| rangeEnd      | 结束时间       | Date    | -      |        |
| selectMode    | 模式           | String  | date   | date   |
| autoPlaySpeed | 自动播放的速度 | Number  | -      | 1000   |

#### Events

| 事件名    | 说明                     | 参数 |
| --------- | ------------------------ | ---- |
| load-date | 加载对应时间点数据的事件 | date |
