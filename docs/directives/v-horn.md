# v-horn

便捷的让容器添加四个角的激活样式的指令

### API

| 名称       | 描述     | 可选值      | 默认值 |
| ---------- | -------- | ----------- | ------ |
| name       | horn     |             |        |
| modifiers  | 交互方式 | hover/click | hover  |
| expression |          |             |        |

#### 示例

```
  <div v-horn class="tn-bordered"></div>
  <div v-horn.click class="tn-bordered"></div>
  <div v-horn="isShowHorn" class="tn-bordered"></div>
```
