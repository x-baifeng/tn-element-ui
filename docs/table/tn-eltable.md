# TnEltable

二次封装的 el-table

### API

#### Name

- 导出名称: `TnEltable`
- 组件名称: `TnEltable`
- 标签名成: `tn-eltable`

#### Attributes

##### el-table 原有功能的支持情况

[el-table 文档](https://element.eleme.cn/#/zh-CN/component/table)

| 原功能 | 支持状态 | 描述 |  
| ------------ | ------- | |  
| Attributes | true | |
| Events | true | |
| Methods | true | this.$refs['tn-eltable'].tableVm |
| Slot | true | |

##### 新增属性

| 名称             | 说明                                      | 类型    | 可选值 | 默认值 |
| ---------------- | ----------------------------------------- | ------- | ------ | ------ |
| columns          | 列配置,支持 Table-column Attributes       | Array   |        | []     |
| empty-cell-value | 空单元格的内容                            | String  |        | --     |
| auto-height      | 自动获取父元素的内容高度作为 Table 的高度 | Boolean |        | true   |

##### column

##### type: button

```
{
  key: 'button',
  label: 'button',
  type: 'button',
  buttons: [
    {
      key: 'update',
      caption: '编辑'
    },
    {
      key: 'delete',
      caption: '删除'
    }
  ]
}
```

##### render

```
Vue.directive('bgcolor', {
  inserted: function (el, { value }) {
    if (value) {
      el.style.backgroundColor = value
      el.style.color = "white"
    }
  }
})
// render column config
{
  key: "render",
  label: "render",
  "min-width": 100,
  render (h, {row, column, index}) {
    return h('div', [
      ...(
        row.value
          ? []
          : [h("el-button", { key: 1 }, "编辑")]
      ),
      ...(
        _this.isShowDelBtn
          ? []
          : [h("el-button", { key: 2 }, "删除")]
      ),
      h("el-button",
        {
          directives: [
            {
              name: 'bgcolor',
              value: row.value ? 'red' : 'blue'
            }
          ],
          key: 3
        },
        "新增"
      )
    ])
  }
}
```

#### Events

| 事件名       | 说明                                   | 参数               |
| ------------ | -------------------------------------- | ------------------ |
| button-click | `type：button`的`column`出发的点击事件 | btnKey, index, row |
