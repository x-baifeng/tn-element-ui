# TnStretchWrapper

伸缩（展开、收起）包裹器

### API

#### Name

- 导出名称: `StretchWrapper`
- 组件名称: `TinyStretchWrapper`
- 标签名成: `tn-stretch-wrapper`

#### Attributes

| 名称                | 描述         | 类型           | 可选值                | 默认值 |
| ------------------- | ------------ | -------------- | --------------------- | ------ |
| width               | 包裹器宽度   | String, Number | -                     | auto   |
| height              | 包裹器高度   | String, Number | -                     | auto   |
| switchPostion       | 开关位置     | String         | left,right,top,bottom | right  |
| switchExpandedStyle | 开关打开样式 | Object         | -                     | {}     |

#### Slots

| 名称               | 描述         | 内容 |
| ------------------ | ------------ | ---- |
| (default)          | 包裹器内容   | Any  |
| (arrow-right-icon) | 右向开关图标 | Any  |
