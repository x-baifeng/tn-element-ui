# tn-element-ui

> `tiny`,`element-ui@^2.15.6`二次封装、文档补全,包含`TnEltable, layout, v-horn，el-tabs`...

[https://gitee.com/x-baifeng/tn-element-ui](https://gitee.com/x-baifeng/tn-element-ui)

## Install

```npm
  npm install tn-element-ui -S
```

## Quick Start

```js
import Vue from "vue";
import "tn-element-ui";
// or
import Vue from "vue";
import * as TnElement from "tn-element-ui";

Vue.use(TnElement);
// or
import { TnEltable } from "tn-element-ui";
Vue.component(TnEltable.name, TnEltable);
```

### Part Importing

```vue
<template>
  <tn-eltable :data="list" :columns="columns" />
</template>

<script>
import { TnEltable } from "tn-element-ui";

const foo = new Vue({
  components: {
    TnEltable,
  },
  data() {
    return {
      data: {
        columns: [],
        list: [],
      },
    };
  },
});
</script>
```
