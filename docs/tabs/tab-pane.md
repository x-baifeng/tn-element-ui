# Tab Pane

补充 tab-pane 文档

### Examples

-

### API

#### Slots

| 名称  | 描述 | 内容 |
| ----- | ---- | ---- |
| label | 标签 | Any  |

```js
const tabs = this._l(panes, (pane, index) => {
  ...
  const tabLabelContent = pane.$slots.label || pane.label;
  ...
})
```
