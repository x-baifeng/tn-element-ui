# Space & Splitter

## Space

空伸缩项

### Examples

-

### API

#### Name

- 导出名称: `Space`
- 组件名称: `TinySpace`
- 标签名成: `tn-space`

#### Extend & Mixins

- Extend: `TinyFlexItem`

#### Attributes

-

## Splitter

分离 flex 容器的单元格并调整其大小

### Examples

-

### API

#### Name

- 导出名称: `Splitter`
- 组件名称: `TinySplitter`
- 标签名成: `tn-splitter`

#### Attributes

| 名称     | 描述      | 类型    | 可选值 | 默认值 |
| -------- | --------- | ------- | ------ | ------ |
| dragable | resizable | Boolean |        | -      |

#### Events

| 名称        | 描述             | arguments |
| ----------- | ---------------- | --------- |
| resize      | 拖动或移动时触发 | -         |
| resizestart | 拖动开始时触发   | -         |
| resizeend   | 拖动结束时触发   | -         |
