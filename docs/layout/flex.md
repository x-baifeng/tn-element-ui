# Flex Layout Components

## Flex Item

伸缩项容器

### Examples

-

### API

#### Name

- 导出名称: `FlexItem`
- 组件名称: `TinyFlexItem`
- 标签名成: `tn-flex-item`

#### Attributes

| 名称        | 描述                           | 类型           | 可选值                                           | 默认值 |
| ----------- | ------------------------------ | -------------- | ------------------------------------------------ | ------ |
| cellpadding | 设置伸缩项的标准内边距         | Boolean        |                                                  | false  |
| cellspacing | 设置伸缩项的标准外边距         | Boolean        |                                                  | false  |
| size        | 设置伸缩项在其布局方向上的大小 | Number, String | px value / percent value / grow multiples / auto | -      |

#### Injection

| 名称         | 描述                   | 默认值 |
| ------------ | ---------------------- | ------ |
| parentLayout | 父级伸缩盒子的布局模式 | row    |

#### Slots

| 名称      | 描述       | 内容 |
| --------- | ---------- | ---- |
| (default) | 单元格内容 | Any  |

## Flex Box

基础伸缩容器

### Examples

-

### API

#### Name

- 导出名称: `FlexBox`
- 组件名称: `TinyFlexBox`
- 标签名成: `tn-flex-box`

#### Extend & Mixins

- Extend: `TinyFlexItem`

#### Attributes

| 名称            | 描述                                     | 类型    | 可选值              | 默认值              |
| --------------- | ---------------------------------------- | ------- | ------------------- | ------------------- |
| layout          | 布局模式                                 | String  | flow / column / row | row                 |
| inline          | inline flex box                          | Boolean |                     | false               |
| flex-wrap       | flex flex-wrap                           | Boolean |                     | flow ? true : false |
| align-items     | flex align-items                         | String  |                     | stretch             |
| justify-content | flex justify-content                     | String  |                     | center              |
| content-spacing | 自动设置容器的标准内边距和子元素的外边距 | Boolean |                     | false               |

#### Slots

| 名称      | 描述   | 内容 |
| --------- | ------ | ---- |
| (default) | 伸缩项 | Any  |

## Flex H Box

横向伸缩盒子(layout: row)

### Examples

-

### API

#### Name

- 导出名称: `HBox`
- 组件名称: `TinyHBox`
- 标签名成: `tn-h-box`

#### Extend & Mixins

- Extend: `TinyFlexBox`

#### Attributes

-

#### Slots

| 名称      | 描述   | 内容 |
| --------- | ------ | ---- |
| (default) | 伸缩项 | Any  |

## Flex V Box

纵向伸缩盒子(layout: column)

### Examples

-

### API

#### Name

- 导出名称: `VBox`
- 组件名称: `TinyVBox`
- 标签名成: `tn-v-box`

#### Extend & Mixins

- Extend: `TinyFlexBox`

#### Attributes

-

#### Slots

| 名称      | 描述   | 内容 |
| --------- | ------ | ---- |
| (default) | 伸缩项 | Any  |
