// $(document).ready(function () {
//   initPage();
// });

var aceEditor;

function initPage() {
  initEditor();
}

//初始化编辑器以及预览内容
function initEditor() {
  loadExampleHtml();
  initCodeEditor();
}

//重置编辑器
function refresh() {
  initEditor();
  run();
}

function createIFrame() {
  var preViewPane = $("#previewPane");
  preViewPane.empty();
  var iframe = document.createElement("iframe");
  $(iframe).attr("id", "innerPage");
  $(iframe).attr("name", "innerPage");
  preViewPane.append(iframe);
  return iframe;
}

function exampleHeight() {
  var doc = $("#innerPage").contents();
  doc.find("html").height("100%");
  doc.find("body").css({
    height: "calc(100% - 20px)",
    margin: 0,
  });
}

//填充预览效果内容
function loadPreview(content) {
  var iFrame = createIFrame(),
      iframeDocument = iFrame.contentWindow.document;
  iFrame.contentWindow.resources=window.resources?window.resources.resources:{};
  iframeDocument.open();
  iframeDocument.write(content);
  iframeDocument.close();
  var doc = document;
  iFrame.addEventListener('load', function () {
      exampleHeight();
      setTimeout(function () {
          doc.title = iframeDocument.title;
      }, 100);
  });

  exampleHeight();
}

//运行代码
function run() {
  var iframeContent = $("#editor").val();
  if (editor) {
      iframeContent = aceEditor.getValue();
  }
  loadPreview(iframeContent);
}

//重置编辑器
function refresh() {
  initEditor();
  run();
}

function getLocationParam() {
  var param = window.location.toString();
  if (param.indexOf("#") === -1) {
      return null;
  }
  param = param.split("#");
  if (param && param.length > 0) {
      return param[1];
  }
}

function loadExampleHtml() {
  var locationParam = getLocationParam();
  if (!locationParam) {
      return;
  }
  var href = window.location.toString();
  var mapUrl = href.substr(0, href.lastIndexOf('/') + 1);
  mapUrl = mapUrl + locationParam + ".html";
  if (!mapUrl) {
      return;
  }
  var html = $.ajax({
      url: mapUrl,
      async: false,
      error: function (error) {
          alert(resources.editor.envTips);
          html = "";
      }
  }).responseText;
  if (html && html != "") {
      $('#editor').val(html);
      loadPreview(html);
  }
}

//初始化编辑器
function initCodeEditor() {
  if (!aceEditor) {
    aceEditor = ace.edit("editor");
    aceEditor.setTheme("ace/theme/textmate");
    aceEditor.getSession().setMode("ace/mode/html");
    aceEditor.getSession().setUseWrapMode(true);
    aceEditor.setShowPrintMargin(false);
    aceEditor.$blockScrolling = Infinity;
  }
  aceEditor.setValue($('#editor').val());
  aceEditor.clearSelection();
  aceEditor.moveCursorTo(0, 0);
}

window.editor = {
  initPage: initPage,
  initEditor: initEditor,
  run: run,
  refresh: refresh
}