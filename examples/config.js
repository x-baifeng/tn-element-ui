var exampleConfig = {
  "element-ui": {
    name: 'element-ui 扩展 | 补充',
    children: [
      {
        name: 'tn-eltable',
        fileName: 'tn-eltable'
      },
      {
        name: 'el-tab-pane',
        fileName: 'el-tab-pane'
      }
    ]
  },
  "Layout": {
    name: '布局',
    children: [
      {
        name: 'layout01',
        fileName: 'layout'
      },
      {
        name: 'layout02',
        fileName: 'layout2'
      },
      {
        name: 'layout-el-form',
        fileName: 'layout-el-form'
      }
    ]
  },
  "Wrapper": {
    name: '包裹器',
    children: [
      {
        name: '伸缩包裹器',
        fileName: 'stretch-wrapper'
      }
    ]
  },
  "VisualUi": {
    name: '可视化',
    children: [
      {
        name: '时间坐标轴播放条',
        fileName: 'visual-ui__time-axis-player'
      }
    ]
  }
}